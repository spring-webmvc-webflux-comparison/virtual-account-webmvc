create table billing (
       id varchar(36) not null,
       active boolean,
       amount numeric(19, 2),
       billing_code varchar(255) not null,
       billing_type varchar(255),
       created_date timestamp,
       cumulative_payment_amount numeric(19, 2),
       customer_email varchar(255),
       customer_phone varchar(20),
       expired_date timestamp,
       inactive_date int8,
       last_payment_amount numeric(19, 2),
       last_payment_date timestamp,
       max_payment_amount numeric(19, 2),
       min_payment_amount numeric(19, 2),
       order_id varchar(255) not null,
       payment_status varchar(15),
       remark varchar(255),
       updated_date timestamp,
       virtual_account_name varchar(255),
       virtual_account_number varchar(255),
       payment_notification_url varchar(255),
       primary key (id)
);

create table billing_transaction (
       id varchar(36) not null,
       cumulative_payment_amount numeric(19, 2),
       payment_amount numeric(19, 2),
       payment_date_time timestamp,
       payment_reference varchar(255),
       transaction_amount numeric(19, 2),
       id_billing varchar(36) not null,
       primary key (id)
);

create index idx_billing on billing (order_id, billing_code, virtual_account_number, payment_status);
alter table billing
       add constraint unq_billing unique (virtual_account_number, active, inactive_date);
alter table billing
       add constraint UK_n8l3h90sgnfnv7yq0nqlp70qb unique (billing_code);
alter table billing
       add constraint UK_je0farg86310qsp6fcwccmv9y unique (order_id);

create index idx_billing_transaction on billing_transaction (id_billing, payment_reference);

alter table billing_transaction
       add constraint FK3q38uaarswcuxoxm0jhoa5gd5
       foreign key (id_billing)
       references billing;

create table transaction_log (
       id varchar(36) not null,
        message_stream TEXT,
        message_type varchar(255),
        order_id varchar(255),
        payment_reference varchar(255),
        response_code varchar(255),
        response_message varchar(255),
        virtual_account_number varchar(255),
        primary key (id)
    );