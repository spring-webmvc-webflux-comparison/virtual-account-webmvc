package com.artivisi.spring.webmvc.webflux.vawebmvc.dao;

import com.artivisi.spring.webmvc.webflux.vawebmvc.domain.Billing;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BillingDao extends PagingAndSortingRepository<Billing, String>{
    
    Optional<Billing> findByBillingCode(String code);
    Optional<Billing> findByOrderId(String orderId);
    Optional<Billing> findByVirtualAccountNumber(String virtualAccountNumber);
    Optional<Billing> findByVirtualAccountName(String virtualAccountName);
    Optional<Billing> findByCustomerEmail(String customerEmail);
    Optional<Billing> findByCustomerPhone(String customerPhone);
    
}
