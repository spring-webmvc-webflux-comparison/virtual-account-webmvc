package com.artivisi.spring.webmvc.webflux.vawebmvc.domain;

import com.artivisi.spring.webmvc.webflux.vawebmvc.constants.BillingType;
import com.artivisi.spring.webmvc.webflux.vawebmvc.constants.PaymentStatus;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Table(indexes = {
        @Index(name = "idx_billing", columnList = "orderId, billingCode, virtualAccountNumber, paymentStatus")
}, uniqueConstraints = @UniqueConstraint(name = "unq_billing", columnNames = {"virtualAccountNumber", "active", "inactiveDate"}))
@Entity @Builder @Data
@AllArgsConstructor @NoArgsConstructor
public class Billing {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(columnDefinition = "varchar(36)")
    private String id;

    @Column(nullable = false, unique = true)
    private String orderId;

    //code billing di generate ketika create billing
    @Column(nullable = false, unique = true)
    private String billingCode;

    @Enumerated(EnumType.STRING)
    private BillingType billingType;

    private String virtualAccountNumber;
    private String virtualAccountName;
    private String customerEmail;
    @Column(length = 20)
    private String customerPhone;
    private String remark;

//    FIXED_PAYMENT = amount to be paid
//    OPEN_PAYMENT = zero amount
//    PARTIAL_PAYMENT = total amount to be paid
//    OPEN_MINIMUM_PAYMENT = minimum amount can be paid
//    OPEN_MAXIMUM_PAYMENT = maximum amount can be paid
    private BigDecimal amount = BigDecimal.ZERO;

    //paid amount
    private BigDecimal lastPaymentAmount = BigDecimal.ZERO;
    private BigDecimal cumulativePaymentAmount = BigDecimal.ZERO;
    private LocalDateTime lastPaymentDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private PaymentStatus paymentStatus;

    private LocalDateTime createdDate;
    private LocalDateTime expiredDate;
    private Boolean active = Boolean.TRUE;
    private LocalDateTime updatedDate;

    //waktu billing inactive
    private Long inactiveDate = 0L;

    // untuk type open min dan min payment amount
    private BigDecimal minPaymentAmount;

    //untuk type open max payment amount
    private BigDecimal maxPaymentAmount;

    private String paymentNotificationUrl; //callback ketika payment success
}