package com.artivisi.spring.webmvc.webflux.vawebmvc.controller;

import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.BaseResponseDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.BillingRequestDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.BillingResponseDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.PaymentRequestDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.PaymentResponseDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.exceptions.BillingException;
import com.artivisi.spring.webmvc.webflux.vawebmvc.services.BillingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public/api/billing")
@Slf4j
public class BillingController {
    
    @Autowired
    private BillingService billingService;
    
    @PostMapping("/create")
    public ResponseEntity<BaseResponseDto> createBilling(@RequestBody BillingRequestDto billingRequestDto) throws BillingException{
        
        BaseResponseDto baseResponseDto = new BaseResponseDto();
        try {
            BillingRequestDto result = billingService.createBilling(billingRequestDto);
            baseResponseDto.setResponseMessage("success");
            baseResponseDto.setResponseCode("00");
            
            BillingResponseDto billingResponseDto = new BillingResponseDto();
            billingResponseDto.setAmount(result.getAmount());
            billingResponseDto.setBillingType(result.getBillingType());
            billingResponseDto.setOrderId(result.getOrderId());
            billingResponseDto.setVirtualAccountName(result.getVirtualAccountName());
            billingResponseDto.setVirtualAccountNumber(result.getVirtualAccountNumber());
            
            baseResponseDto.setData(billingResponseDto);
            return ResponseEntity.ok(baseResponseDto);
        } catch (BillingException b) {
            
            log.error("Error system : ", b);
            baseResponseDto.setResponseMessage(b.getMessage());
            baseResponseDto.setResponseCode(b.getErrorCode());
            baseResponseDto.setData(null);
            
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(baseResponseDto);
        } catch (Exception e) {
            
            log.error("Error system : ", e);
            baseResponseDto.setResponseMessage("Sistem sedang error");
            baseResponseDto.setResponseCode("500");
            baseResponseDto.setData(null);
            
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(baseResponseDto);
        }
    }
    
    @PostMapping("/payment")
    public ResponseEntity<BaseResponseDto> paymentBilling(@RequestBody PaymentRequestDto paymentRequestDto) throws BillingException{
        
        BaseResponseDto baseResponseDto = new BaseResponseDto();
        try {
            PaymentRequestDto result = billingService.paymentBilling(paymentRequestDto);
            baseResponseDto.setResponseMessage("success");
            baseResponseDto.setResponseCode("00");
            
            PaymentResponseDto paymentResponseDto = new PaymentResponseDto();
            paymentResponseDto.setBillingCode(result.getBillingCode());
            paymentResponseDto.setVirtualAccountNumber(result.getVirtualAccountNumber());
            
            baseResponseDto.setData(paymentResponseDto);
            
            return ResponseEntity.ok(baseResponseDto);
        } catch (BillingException b) {    
            log.error("Error system : ", b);
            baseResponseDto.setResponseMessage(b.getMessage());
            baseResponseDto.setResponseCode(b.getErrorCode());
            baseResponseDto.setData(null);
            
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(baseResponseDto);
        } catch (Exception e) {
            baseResponseDto.setResponseMessage(e.getMessage());
            baseResponseDto.setResponseCode("500");
            baseResponseDto.setData(null);
            
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(baseResponseDto);
        }
    }
}
