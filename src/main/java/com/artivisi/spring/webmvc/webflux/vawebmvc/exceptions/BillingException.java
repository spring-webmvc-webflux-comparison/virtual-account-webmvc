package com.artivisi.spring.webmvc.webflux.vawebmvc.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

public class BillingException extends Exception {

    private @Getter @Setter String errorCode;
    private @Getter @Setter HttpStatus httpStatusCode;

    public BillingException() {
    }

    public BillingException(String message) {
        super(message);
    }

    public BillingException(String errorCode, String message, HttpStatus httpStatusCode) {
        super(message);
        this.errorCode = errorCode;
        this.httpStatusCode = httpStatusCode;
    }

    public BillingException(String message, Throwable cause) {
        super(message, cause);
    }

    public BillingException(String errorCode, String message, HttpStatus httpStatusCode, Throwable cause) {
        super(message);
        this.errorCode = errorCode;
        this.httpStatusCode = httpStatusCode;
    }

    public BillingException(Throwable cause) {
        super(cause);
    }

    public BillingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

