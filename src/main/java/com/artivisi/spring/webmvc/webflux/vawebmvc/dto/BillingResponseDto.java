package com.artivisi.spring.webmvc.webflux.vawebmvc.dto;

import com.artivisi.spring.webmvc.webflux.vawebmvc.constants.BillingType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import lombok.Builder;

@Data @Builder
@AllArgsConstructor @NoArgsConstructor
public class BillingResponseDto {
    private String orderId;
    private String billingCode;
    private String virtualAccountNumber;
    private String virtualAccountName;
    private BillingType billingType;
    private BigDecimal amount;
}