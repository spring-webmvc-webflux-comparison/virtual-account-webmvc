package com.artivisi.spring.webmvc.webflux.vawebmvc.constants;

public enum BillingType {
    FIXED_PAYMENT,
    OPEN_PAYMENT,
    PARTIAL_PAYMENT,
    OPEN_MINIMUM_PAYMENT,
    OPEN_MAXIMUM_PAYMENT
}
