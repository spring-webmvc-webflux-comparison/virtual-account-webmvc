package com.artivisi.spring.webmvc.webflux.vawebmvc.constants;

public enum  TransactionLogType {
    BILLING_REQUEST, BILLING_RESPONSE, PAYMENT_REQUEST, PAYMENT_RESPONSE
}
