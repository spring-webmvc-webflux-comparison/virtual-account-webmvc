package com.artivisi.spring.webmvc.webflux.vawebmvc.domain;

import com.artivisi.spring.webmvc.webflux.vawebmvc.constants.TransactionLogType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data @Table @Entity
@AllArgsConstructor @NoArgsConstructor
public class TransactionLog {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(columnDefinition = "varchar(36)")
    private String id;

    private String virtualAccountNumber;
    private String orderId;
    private String paymentReference;

    @Enumerated(EnumType.STRING)
    private TransactionLogType messageType;

    @Lob
    @Column(columnDefinition = "TEXT")
    private String messageStream;

    private String responseCode;
    private String responseMessage;
}