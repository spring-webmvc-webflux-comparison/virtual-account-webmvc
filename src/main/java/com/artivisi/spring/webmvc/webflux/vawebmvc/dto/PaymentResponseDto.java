package com.artivisi.spring.webmvc.webflux.vawebmvc.dto;

import com.artivisi.spring.webmvc.webflux.vawebmvc.constants.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor @NoArgsConstructor
public class PaymentResponseDto {
    private String billingCode;
    private String virtualAccountNumber;
    private String paymentReference;
}