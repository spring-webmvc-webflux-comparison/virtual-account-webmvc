package com.artivisi.spring.webmvc.webflux.vawebmvc.constants;

public enum  PaymentStatus {
    PAID, UNPAID, UNDERPAID
}
