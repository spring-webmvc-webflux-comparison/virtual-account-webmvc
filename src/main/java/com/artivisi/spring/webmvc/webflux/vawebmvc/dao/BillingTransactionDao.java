package com.artivisi.spring.webmvc.webflux.vawebmvc.dao;

import com.artivisi.spring.webmvc.webflux.vawebmvc.domain.BillingTransaction;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BillingTransactionDao extends PagingAndSortingRepository<BillingTransaction, String>{
    
    
    
}
