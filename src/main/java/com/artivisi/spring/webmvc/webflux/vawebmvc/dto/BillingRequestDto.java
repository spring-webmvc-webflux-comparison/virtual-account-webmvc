package com.artivisi.spring.webmvc.webflux.vawebmvc.dto;

import com.artivisi.spring.webmvc.webflux.vawebmvc.constants.BillingType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor @NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BillingRequestDto {

    private String orderId;
    private BigDecimal amount;
    private String virtualAccountNumber;
    private String virtualAccountName;
    private String customerEmail;
    private String customerPhoneNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime expiredDate;

    private BillingType billingType;
    private String paymentNotificationUrl;
}