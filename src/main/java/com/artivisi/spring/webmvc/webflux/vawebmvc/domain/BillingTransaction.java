package com.artivisi.spring.webmvc.webflux.vawebmvc.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Table(indexes = @Index(name = "idx_billing_transaction", columnList = "id_billing, paymentReference"))
@Entity
@Data @Builder
@NoArgsConstructor @AllArgsConstructor
public class BillingTransaction {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(columnDefinition = "varchar(36)")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_billing", nullable = false)
    private Billing billing;

    private BigDecimal paymentAmount; //paid amount
    private BigDecimal cumulativePaymentAmount;
    private BigDecimal transactionAmount; //billing amount
    private String paymentReference; //payment reference / jurnal num
    private LocalDateTime paymentDateTime;
}