package com.artivisi.spring.webmvc.webflux.vawebmvc.dao;

import com.artivisi.spring.webmvc.webflux.vawebmvc.domain.TransactionLog;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TransactionLogDao extends PagingAndSortingRepository<TransactionLog, String>{
    
}
