package com.artivisi.spring.webmvc.webflux.vawebmvc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class BaseResponseDto {
    private String responseCode;
    private String responseMessage;
    private Object data;
}