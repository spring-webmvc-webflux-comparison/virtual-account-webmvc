package com.artivisi.spring.webmvc.webflux.vawebmvc.services;

import com.artivisi.spring.webmvc.webflux.vawebmvc.constants.PaymentStatus;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dao.BillingDao;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dao.BillingTransactionDao;
import com.artivisi.spring.webmvc.webflux.vawebmvc.domain.Billing;
import com.artivisi.spring.webmvc.webflux.vawebmvc.domain.BillingTransaction;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.BillingRequestDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.PaymentRequestDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.exceptions.BillingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BillingService {
    
    @Autowired private BillingDao billingDao;
    @Autowired private BillingTransactionDao billingTransactionDao;
    @Autowired private TransactionLogService transactionLogService;
   
    public BillingRequestDto createBilling(BillingRequestDto billingRequestDto) throws BillingException{
        
        //save transaction log billing request
        transactionLogService.createTransactionLogBillingRequest(billingRequestDto);
        
       
        try {
            Optional<Billing> orderIdBilling = billingDao.findByOrderId(billingRequestDto.getOrderId());
            if (orderIdBilling.isPresent()) {
                throw new BillingException("500", "Order Id has been used !", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            Optional<Billing> virtualAccountNumber = billingDao.findByVirtualAccountNumber(billingRequestDto.getVirtualAccountNumber());
            if(virtualAccountNumber.isPresent()){
                throw new BillingException("500", "Virtual account number has been used !", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            Optional<Billing> virtualAccountName = billingDao.findByVirtualAccountName(billingRequestDto.getVirtualAccountName());
            if(virtualAccountName.isPresent()){
                throw new BillingException("500", "Virtual account name has been used !", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        
            Billing billing = new Billing();
            billing.setOrderId(billingRequestDto.getOrderId());
            billing.setBillingCode(UUID.randomUUID().toString());
            billing.setBillingType(billingRequestDto.getBillingType());
            billing.setAmount(billingRequestDto.getAmount());
            billing.setVirtualAccountNumber(billingRequestDto.getVirtualAccountNumber());
            billing.setVirtualAccountName(billingRequestDto.getVirtualAccountName());
            billing.setCustomerEmail(billingRequestDto.getCustomerEmail());
            billing.setCustomerPhone(billingRequestDto.getCustomerPhoneNumber());
            billing.setExpiredDate(billingRequestDto.getExpiredDate());
            billing.setPaymentNotificationUrl(billingRequestDto.getPaymentNotificationUrl());
            billing.setPaymentStatus(PaymentStatus.UNPAID);
            billing.setCreatedDate(LocalDateTime.now());
            
            billingDao.save(billing);
            
            //save transaction log billing response
            transactionLogService.createTransactionLogBillingResponse(billingRequestDto,null);
            
        }catch (BillingException b) {
            transactionLogService.createTransactionLogBillingResponse(billingRequestDto, b);
            throw b;
        } catch (Exception e) {
            transactionLogService.createTransactionLogBillingResponse(billingRequestDto, e);
            throw e;
        }
        
        return billingRequestDto;
        
    }
    
    public PaymentRequestDto paymentBilling(PaymentRequestDto paymentRequestDto) throws BillingException {
        
        //save transaction log payment request
        transactionLogService.createTransactionLogPaymentRequest(paymentRequestDto);
        
        try {
            BillingTransaction billingTransaction = new BillingTransaction();
        
            Optional<Billing> billing = billingDao.findByBillingCode(paymentRequestDto.getBillingCode());
            
            if(billing.get().getActive() == Boolean.FALSE && billing.get().getPaymentStatus().equals(PaymentStatus.PAID)){
                throw new BillingException("500", "Billing ini sudah terbayar!", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            
            if (billing.isPresent()) {
                billingTransaction.setBilling(billing.get());
                billing.get().setActive(Boolean.FALSE);
                billing.get().setPaymentStatus(PaymentStatus.PAID);
                billing.get().setInactiveDate(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());
            } else {
                throw new BillingException("500", "Client not found", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            billingTransaction.setPaymentReference(UUID.randomUUID().toString());
            billingTransaction.setPaymentDateTime(LocalDateTime.now());
            billingTransaction.setTransactionAmount(billing.get().getAmount());

            billingTransactionDao.save(billingTransaction);
            
            //save transaction log payment response
            transactionLogService.createTransactionLogPaymentResponse(paymentRequestDto, null);
        } catch (BillingException b) {
            transactionLogService.createTransactionLogPaymentResponse(paymentRequestDto, b);
            throw b;
        } catch (Exception e) {
            transactionLogService.createTransactionLogPaymentResponse(paymentRequestDto, e);
            throw e;
        }
        
        return paymentRequestDto;
        
    } 
}