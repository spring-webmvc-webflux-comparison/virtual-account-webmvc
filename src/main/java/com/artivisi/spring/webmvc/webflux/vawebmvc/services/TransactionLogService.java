package com.artivisi.spring.webmvc.webflux.vawebmvc.services;

import com.artivisi.spring.webmvc.webflux.vawebmvc.constants.TransactionLogType;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dao.BillingDao;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dao.TransactionLogDao;
import com.artivisi.spring.webmvc.webflux.vawebmvc.domain.Billing;
import com.artivisi.spring.webmvc.webflux.vawebmvc.domain.TransactionLog;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.BillingRequestDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.dto.PaymentRequestDto;
import com.artivisi.spring.webmvc.webflux.vawebmvc.exceptions.BillingException;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TransactionLogService {
    
    @Autowired
    BillingDao billingDao;
    @Autowired
    TransactionLogDao transactionLogDao;
    
    public TransactionLog createTransactionLogBillingRequest(BillingRequestDto billingRequestDto){
        
        TransactionLog transactionLog = new TransactionLog();
        
        transactionLog.setOrderId(billingRequestDto.getOrderId());
        transactionLog.setVirtualAccountNumber(billingRequestDto.getVirtualAccountNumber());
        transactionLog.setMessageType(TransactionLogType.BILLING_REQUEST);

        transactionLogDao.save(transactionLog);
        return transactionLog;
        
    }
    
    public void createTransactionLogBillingResponse(BillingRequestDto billingRequestDto, Exception e){
        
        TransactionLog transactionLog = new TransactionLog();
        transactionLog.setOrderId(billingRequestDto.getOrderId());
        transactionLog.setVirtualAccountNumber(billingRequestDto.getVirtualAccountNumber());
        transactionLog.setMessageType(TransactionLogType.BILLING_RESPONSE);
        if(e != null){
            if(e instanceof BillingException){
                BillingException b = (BillingException) e;
                transactionLog.setResponseMessage(b.getMessage());
                transactionLog.setResponseCode(b.getErrorCode());
                transactionLog.setResponseCode(b.getHttpStatusCode().toString());
            }else{
                transactionLog.setResponseMessage(e.getMessage());
                
            }
        }
        transactionLogDao.save(transactionLog);
    }
    
    public void createTransactionLogPaymentRequest(PaymentRequestDto paymentRequestDto){
        
        TransactionLog transactionLog = new TransactionLog();
        Optional<Billing> billing = billingDao.findByBillingCode(paymentRequestDto.getBillingCode());
        transactionLog.setOrderId(billing.get().getOrderId());
        transactionLog.setVirtualAccountNumber(billing.get().getVirtualAccountNumber());
        transactionLog.setMessageType(TransactionLogType.PAYMENT_REQUEST);

        transactionLogDao.save(transactionLog);
    }
    
    public void createTransactionLogPaymentResponse(PaymentRequestDto paymentRequestDto, Exception e){
        
        TransactionLog transactionLog = new TransactionLog();
        
        Optional<Billing> billing = billingDao.findByBillingCode(paymentRequestDto.getBillingCode());
        transactionLog.setOrderId(billing.get().getOrderId());
        transactionLog.setVirtualAccountNumber(billing.get().getVirtualAccountNumber());
        transactionLog.setMessageType(TransactionLogType.PAYMENT_RESPONSE);
        
        if(e != null){
            if(e instanceof BillingException){
                BillingException b = (BillingException) e;
                transactionLog.setResponseMessage(b.getMessage());
                transactionLog.setResponseCode(b.getErrorCode());
                transactionLog.setMessageStream(b.getHttpStatusCode().toString());
            }else{
                transactionLog.setResponseMessage(e.getMessage());
                
            }
        }
        
        transactionLogDao.save(transactionLog);
    }
}
